module rocm.hip_runtime_api;

extern (C):
@nogc:
nothrow:

enum
{
    HIP_SUCCESS = 0,
    HIP_ERROR_INVALID_VALUE = 1,
    HIP_ERROR_NOT_INITIALIZED = 2,
    HIP_ERROR_LAUNCH_OUT_OF_RESOURCES = 3
}

struct hipDeviceArch_t
{
    import std.bitmanip : bitfields;

    mixin(bitfields!(uint, "hasGlobalInt32Atomics", 1, uint, "hasGlobalFloatAtomicExch", 1, uint,
            "hasSharedInt32Atomics", 1, uint, "hasSharedFloatAtomicExch", 1, uint, "hasFloatAtomicAdd", 1, uint,
            "hasGlobalInt64Atomics", 1, uint, "hasSharedInt64Atomics", 1, uint, "hasDoubles", 1, uint,
            "hasWarpVote", 1, uint, "hasWarpBallot", 1, uint, "hasWarpShuffle", 1, uint, "hasFunnelShift",
            1, uint, "hasThreadFenceSystem", 1, uint, "hasSyncThreadsExt", 1, uint,
            "hasSurfaceFuncs", 1, uint, "has3dGrid", 1, uint,
            "hasDynamicParallelism", 1, uint, "", 15));
}

struct hipUUID_t
{
    char[16] bytes;
}

alias hipUUID = hipUUID_t;

struct hipDeviceProp_t
{
    char[256] name;
    size_t totalGlobalMem;
    size_t sharedMemPerBlock;
    int regsPerBlock;
    int warpSize;
    int maxThreadsPerBlock;
    int[3] maxThreadsDim;
    int[3] maxGridSize;
    int clockRate;
    int memoryClockRate;
    int memoryBusWidth;
    size_t totalConstMem;
    int major;

    int minor;

    int multiProcessorCount;
    int l2CacheSize;
    int maxThreadsPerMultiProcessor;
    int computeMode;
    int clockInstructionRate;

    hipDeviceArch_t arch;
    int concurrentKernels;
    int pciDomainID;
    int pciBusID;
    int pciDeviceID;
    size_t maxSharedMemoryPerMultiProcessor;
    int isMultiGpuBoard;
    int canMapHostMemory;
    int gcnArch;
    char[256] gcnArchName;
    int integrated;
    int cooperativeLaunch;
    int cooperativeMultiDeviceLaunch;
    int maxTexture1DLinear;
    int maxTexture1D;
    int[2] maxTexture2D;
    int[3] maxTexture3D;
    uint* hdpMemFlushCntl;
    uint* hdpRegFlushCntl;
    size_t memPitch;
    size_t textureAlignment;
    size_t texturePitchAlignment;
    int kernelExecTimeoutEnabled;
    int ECCEnabled;
    int tccDriver;
    int cooperativeMultiDeviceUnmatchedFunc;

    int cooperativeMultiDeviceUnmatchedGridDim;

    int cooperativeMultiDeviceUnmatchedBlockDim;

    int cooperativeMultiDeviceUnmatchedSharedMem;

    int isLargeBar;
    int asicRevision;
    int managedMemory;
    int directManagedMemAccessFromHost;
    int concurrentManagedAccess;
    int pageableMemoryAccess;

    int pageableMemoryAccessUsesHostPageTables;
}

enum hipMemoryType
{
    hipMemoryTypeHost = 0,
    hipMemoryTypeDevice = 1,

    hipMemoryTypeArray = 2,

    hipMemoryTypeUnified = 3
}

enum hipKernelNodeAttrID
{
    hipKernelNodeAttributeAccessPolicyWindow = 1,
    hipKernelNodeAttributeCooperative = 2
}

enum hipAccessProperty
{
    hipAccessPropertyNormal = 0,
    hipAccessPropertyStreaming = 1,
    hipAccessPropertyPersisting = 2
}

struct hipAccessPolicyWindow
{
    void* base_ptr;
    hipAccessProperty hitProp;
    float hitRatio;
    hipAccessProperty missProp;
    size_t num_bytes;
}

union hipKernelNodeAttrValue
{
    hipAccessPolicyWindow accessPolicyWindow;
    int cooperative;
}

struct hipPointerAttribute_t
{
    hipMemoryType memoryType;
    int device;
    void* devicePointer;
    void* hostPointer;
    int isManaged;
    uint allocationFlags;
}

enum hipError_t
{
    hipSuccess = 0,
    hipErrorInvalidValue = 1,

    hipErrorOutOfMemory = 2,

    hipErrorMemoryAllocation = 2,
    hipErrorNotInitialized = 3,

    hipErrorInitializationError = 3,
    hipErrorDeinitialized = 4,
    hipErrorProfilerDisabled = 5,
    hipErrorProfilerNotInitialized = 6,
    hipErrorProfilerAlreadyStarted = 7,
    hipErrorProfilerAlreadyStopped = 8,
    hipErrorInvalidConfiguration = 9,
    hipErrorInvalidPitchValue = 12,
    hipErrorInvalidSymbol = 13,
    hipErrorInvalidDevicePointer = 17,
    hipErrorInvalidMemcpyDirection = 21,
    hipErrorInsufficientDriver = 35,
    hipErrorMissingConfiguration = 52,
    hipErrorPriorLaunchFailure = 53,
    hipErrorInvalidDeviceFunction = 98,
    hipErrorNoDevice = 100,
    hipErrorInvalidDevice = 101,
    hipErrorInvalidImage = 200,
    hipErrorInvalidContext = 201,
    hipErrorContextAlreadyCurrent = 202,
    hipErrorMapFailed = 205,

    hipErrorMapBufferObjectFailed = 205,
    hipErrorUnmapFailed = 206,
    hipErrorArrayIsMapped = 207,
    hipErrorAlreadyMapped = 208,
    hipErrorNoBinaryForGpu = 209,
    hipErrorAlreadyAcquired = 210,
    hipErrorNotMapped = 211,
    hipErrorNotMappedAsArray = 212,
    hipErrorNotMappedAsPointer = 213,
    hipErrorECCNotCorrectable = 214,
    hipErrorUnsupportedLimit = 215,
    hipErrorContextAlreadyInUse = 216,
    hipErrorPeerAccessUnsupported = 217,
    hipErrorInvalidKernelFile = 218,
    hipErrorInvalidGraphicsContext = 219,
    hipErrorInvalidSource = 300,
    hipErrorFileNotFound = 301,
    hipErrorSharedObjectSymbolNotFound = 302,
    hipErrorSharedObjectInitFailed = 303,
    hipErrorOperatingSystem = 304,
    hipErrorInvalidHandle = 400,

    hipErrorInvalidResourceHandle = 400,
    hipErrorIllegalState = 401,
    hipErrorNotFound = 500,
    hipErrorNotReady = 600,

    hipErrorIllegalAddress = 700,
    hipErrorLaunchOutOfResources = 701,
    hipErrorLaunchTimeOut = 702,
    hipErrorPeerAccessAlreadyEnabled = 704,
    hipErrorPeerAccessNotEnabled = 705,
    hipErrorSetOnActiveProcess = 708,
    hipErrorContextIsDestroyed = 709,
    hipErrorAssert = 710,
    hipErrorHostMemoryAlreadyRegistered = 712,
    hipErrorHostMemoryNotRegistered = 713,
    hipErrorLaunchFailure = 719,
    hipErrorCooperativeLaunchTooLarge = 720,

    hipErrorNotSupported = 801,
    hipErrorStreamCaptureUnsupported = 900,

    hipErrorStreamCaptureInvalidated = 901,

    hipErrorStreamCaptureMerge = 902,

    hipErrorStreamCaptureUnmatched = 903,
    hipErrorStreamCaptureUnjoined = 904,

    hipErrorStreamCaptureIsolation = 905,

    hipErrorStreamCaptureImplicit = 906,

    hipErrorCapturedEvent = 907,

    hipErrorStreamCaptureWrongThread = 908,

    hipErrorGraphExecUpdateFailure = 910,

    hipErrorUnknown = 999,

    hipErrorRuntimeMemory = 1052,

    hipErrorRuntimeOther = 1053,

    hipErrorTbd = 1054
}

enum hipDeviceAttribute_t
{
    hipDeviceAttributeCudaCompatibleBegin = 0,

    hipDeviceAttributeEccEnabled = hipDeviceAttributeCudaCompatibleBegin,
    hipDeviceAttributeAccessPolicyMaxWindowSize = 1,
    hipDeviceAttributeAsyncEngineCount = 2,
    hipDeviceAttributeCanMapHostMemory = 3,
    hipDeviceAttributeCanUseHostPointerForRegisteredMem = 4,

    hipDeviceAttributeClockRate = 5,
    hipDeviceAttributeComputeMode = 6,
    hipDeviceAttributeComputePreemptionSupported = 7,
    hipDeviceAttributeConcurrentKernels = 8,
    hipDeviceAttributeConcurrentManagedAccess = 9,
    hipDeviceAttributeCooperativeLaunch = 10,
    hipDeviceAttributeCooperativeMultiDeviceLaunch = 11,
    hipDeviceAttributeDeviceOverlap = 12,

    hipDeviceAttributeDirectManagedMemAccessFromHost = 13,

    hipDeviceAttributeGlobalL1CacheSupported = 14,
    hipDeviceAttributeHostNativeAtomicSupported = 15,
    hipDeviceAttributeIntegrated = 16,
    hipDeviceAttributeIsMultiGpuBoard = 17,
    hipDeviceAttributeKernelExecTimeout = 18,
    hipDeviceAttributeL2CacheSize = 19,
    hipDeviceAttributeLocalL1CacheSupported = 20,
    hipDeviceAttributeLuid = 21,
    hipDeviceAttributeLuidDeviceNodeMask = 22,
    hipDeviceAttributeComputeCapabilityMajor = 23,
    hipDeviceAttributeManagedMemory = 24,
    hipDeviceAttributeMaxBlocksPerMultiProcessor = 25,
    hipDeviceAttributeMaxBlockDimX = 26,
    hipDeviceAttributeMaxBlockDimY = 27,
    hipDeviceAttributeMaxBlockDimZ = 28,
    hipDeviceAttributeMaxGridDimX = 29,
    hipDeviceAttributeMaxGridDimY = 30,
    hipDeviceAttributeMaxGridDimZ = 31,
    hipDeviceAttributeMaxSurface1D = 32,
    hipDeviceAttributeMaxSurface1DLayered = 33,
    hipDeviceAttributeMaxSurface2D = 34,
    hipDeviceAttributeMaxSurface2DLayered = 35,
    hipDeviceAttributeMaxSurface3D = 36,
    hipDeviceAttributeMaxSurfaceCubemap = 37,
    hipDeviceAttributeMaxSurfaceCubemapLayered = 38,
    hipDeviceAttributeMaxTexture1DWidth = 39,
    hipDeviceAttributeMaxTexture1DLayered = 40,
    hipDeviceAttributeMaxTexture1DLinear = 41,

    hipDeviceAttributeMaxTexture1DMipmap = 42,
    hipDeviceAttributeMaxTexture2DWidth = 43,
    hipDeviceAttributeMaxTexture2DHeight = 44,
    hipDeviceAttributeMaxTexture2DGather = 45,
    hipDeviceAttributeMaxTexture2DLayered = 46,
    hipDeviceAttributeMaxTexture2DLinear = 47,
    hipDeviceAttributeMaxTexture2DMipmap = 48,
    hipDeviceAttributeMaxTexture3DWidth = 49,
    hipDeviceAttributeMaxTexture3DHeight = 50,
    hipDeviceAttributeMaxTexture3DDepth = 51,
    hipDeviceAttributeMaxTexture3DAlt = 52,
    hipDeviceAttributeMaxTextureCubemap = 53,
    hipDeviceAttributeMaxTextureCubemapLayered = 54,
    hipDeviceAttributeMaxThreadsDim = 55,
    hipDeviceAttributeMaxThreadsPerBlock = 56,
    hipDeviceAttributeMaxThreadsPerMultiProcessor = 57,
    hipDeviceAttributeMaxPitch = 58,
    hipDeviceAttributeMemoryBusWidth = 59,
    hipDeviceAttributeMemoryClockRate = 60,
    hipDeviceAttributeComputeCapabilityMinor = 61,
    hipDeviceAttributeMultiGpuBoardGroupID = 62,
    hipDeviceAttributeMultiprocessorCount = 63,
    hipDeviceAttributeName = 64,
    hipDeviceAttributePageableMemoryAccess = 65,

    hipDeviceAttributePageableMemoryAccessUsesHostPageTables = 66,
    hipDeviceAttributePciBusId = 67,
    hipDeviceAttributePciDeviceId = 68,
    hipDeviceAttributePciDomainID = 69,
    hipDeviceAttributePersistingL2CacheMaxSize = 70,
    hipDeviceAttributeMaxRegistersPerBlock = 71,

    hipDeviceAttributeMaxRegistersPerMultiprocessor = 72,
    hipDeviceAttributeReservedSharedMemPerBlock = 73,
    hipDeviceAttributeMaxSharedMemoryPerBlock = 74,
    hipDeviceAttributeSharedMemPerBlockOptin = 75,
    hipDeviceAttributeSharedMemPerMultiprocessor = 76,
    hipDeviceAttributeSingleToDoublePrecisionPerfRatio = 77,
    hipDeviceAttributeStreamPrioritiesSupported = 78,
    hipDeviceAttributeSurfaceAlignment = 79,
    hipDeviceAttributeTccDriver = 80,
    hipDeviceAttributeTextureAlignment = 81,
    hipDeviceAttributeTexturePitchAlignment = 82,
    hipDeviceAttributeTotalConstantMemory = 83,
    hipDeviceAttributeTotalGlobalMem = 84,
    hipDeviceAttributeUnifiedAddressing = 85,
    hipDeviceAttributeUuid = 86,
    hipDeviceAttributeWarpSize = 87,
    hipDeviceAttributeMemoryPoolsSupported = 88,

    hipDeviceAttributeCudaCompatibleEnd = 9999,
    hipDeviceAttributeAmdSpecificBegin = 10000,

    hipDeviceAttributeClockInstructionRate = hipDeviceAttributeAmdSpecificBegin,
    hipDeviceAttributeArch = 10001,
    hipDeviceAttributeMaxSharedMemoryPerMultiprocessor = 10002,
    hipDeviceAttributeGcnArch = 10003,
    hipDeviceAttributeGcnArchName = 10004,
    hipDeviceAttributeHdpMemFlushCntl = 10005,
    hipDeviceAttributeHdpRegFlushCntl = 10006,
    hipDeviceAttributeCooperativeMultiDeviceUnmatchedFunc = 10007,

    hipDeviceAttributeCooperativeMultiDeviceUnmatchedGridDim = 10008,

    hipDeviceAttributeCooperativeMultiDeviceUnmatchedBlockDim = 10009,

    hipDeviceAttributeCooperativeMultiDeviceUnmatchedSharedMem = 10010,

    hipDeviceAttributeIsLargeBar = 10011,
    hipDeviceAttributeAsicRevision = 10012,
    hipDeviceAttributeCanUseStreamWaitValue = 10013,

    hipDeviceAttributeImageSupport = 10014,
    hipDeviceAttributePhysicalMultiProcessorCount = 10015,

    hipDeviceAttributeFineGrainSupport = 10016,

    hipDeviceAttributeAmdSpecificEnd = 19999,
    hipDeviceAttributeVendorSpecificBegin = 20000
}

enum hipComputeMode
{
    hipComputeModeDefault = 0,
    hipComputeModeExclusive = 1,
    hipComputeModeProhibited = 2,
    hipComputeModeExclusiveProcess = 3
}

alias hipDeviceptr_t = void*;

enum hipChannelFormatKind
{
    hipChannelFormatKindSigned = 0,
    hipChannelFormatKindUnsigned = 1,
    hipChannelFormatKindFloat = 2,
    hipChannelFormatKindNone = 3
}

struct hipChannelFormatDesc
{
    int x;
    int y;
    int z;
    int w;
    hipChannelFormatKind f;
}

enum hipArray_Format
{
    HIP_AD_FORMAT_UNSIGNED_INT8 = 0x01,
    HIP_AD_FORMAT_UNSIGNED_INT16 = 0x02,
    HIP_AD_FORMAT_UNSIGNED_INT32 = 0x03,
    HIP_AD_FORMAT_SIGNED_INT8 = 0x08,
    HIP_AD_FORMAT_SIGNED_INT16 = 0x09,
    HIP_AD_FORMAT_SIGNED_INT32 = 0x0a,
    HIP_AD_FORMAT_HALF = 0x10,
    HIP_AD_FORMAT_FLOAT = 0x20
}

struct HIP_ARRAY_DESCRIPTOR
{
    size_t Width;
    size_t Height;
    hipArray_Format Format;
    uint NumChannels;
}

struct HIP_ARRAY3D_DESCRIPTOR
{
    size_t Width;
    size_t Height;
    size_t Depth;
    hipArray_Format Format;
    uint NumChannels;
    uint Flags;
}

struct hipArray
{
    void* data;
    hipChannelFormatDesc desc;
    uint type;
    uint width;
    uint height;
    uint depth;
    hipArray_Format Format;
    uint NumChannels;
    bool isDrv;
    uint textureType;
}

struct hip_Memcpy2D
{
    size_t srcXInBytes;
    size_t srcY;
    hipMemoryType srcMemoryType;
    const(void)* srcHost;
    hipDeviceptr_t srcDevice;
    hipArray* srcArray;
    size_t srcPitch;
    size_t dstXInBytes;
    size_t dstY;
    hipMemoryType dstMemoryType;
    void* dstHost;
    hipDeviceptr_t dstDevice;
    hipArray* dstArray;
    size_t dstPitch;
    size_t WidthInBytes;
    size_t Height;
}

alias hipArray_t = hipArray*;
alias hiparray = hipArray*;
alias hipArray_const_t = const(hipArray)*;

struct hipMipmappedArray
{
    void* data;
    hipChannelFormatDesc desc;
    uint type;
    uint width;
    uint height;
    uint depth;
    uint min_mipmap_level;
    uint max_mipmap_level;
    uint flags;
    hipArray_Format format;
}

alias hipMipmappedArray_t = hipMipmappedArray*;
alias hipMipmappedArray_const_t = const(hipMipmappedArray)*;

enum hipResourceType
{
    hipResourceTypeArray = 0x00,
    hipResourceTypeMipmappedArray = 0x01,
    hipResourceTypeLinear = 0x02,
    hipResourceTypePitch2D = 0x03
}

enum HIPresourcetype_enum
{
    HIP_RESOURCE_TYPE_ARRAY = 0x00,
    HIP_RESOURCE_TYPE_MIPMAPPED_ARRAY = 0x01,
    HIP_RESOURCE_TYPE_LINEAR = 0x02,
    HIP_RESOURCE_TYPE_PITCH2D = 0x03
}

alias HIPresourcetype = HIPresourcetype_enum;

enum HIPaddress_mode_enum
{
    HIP_TR_ADDRESS_MODE_WRAP = 0,
    HIP_TR_ADDRESS_MODE_CLAMP = 1,
    HIP_TR_ADDRESS_MODE_MIRROR = 2,
    HIP_TR_ADDRESS_MODE_BORDER = 3
}

alias HIPaddress_mode = HIPaddress_mode_enum;

enum HIPfilter_mode_enum
{
    HIP_TR_FILTER_MODE_POINT = 0,
    HIP_TR_FILTER_MODE_LINEAR = 1
}

alias HIPfilter_mode = HIPfilter_mode_enum;

struct HIP_TEXTURE_DESC_st
{
    HIPaddress_mode[3] addressMode;
    HIPfilter_mode filterMode;
    uint flags;
    uint maxAnisotropy;
    HIPfilter_mode mipmapFilterMode;
    float mipmapLevelBias;
    float minMipmapLevelClamp;
    float maxMipmapLevelClamp;
    float[4] borderColor;
    int[12] reserved;
}

alias HIP_TEXTURE_DESC = HIP_TEXTURE_DESC_st;

enum hipResourceViewFormat
{
    hipResViewFormatNone = 0x00,
    hipResViewFormatUnsignedChar1 = 0x01,
    hipResViewFormatUnsignedChar2 = 0x02,
    hipResViewFormatUnsignedChar4 = 0x03,
    hipResViewFormatSignedChar1 = 0x04,
    hipResViewFormatSignedChar2 = 0x05,
    hipResViewFormatSignedChar4 = 0x06,
    hipResViewFormatUnsignedShort1
        = 0x07, hipResViewFormatUnsignedShort2 = 0x08,
        hipResViewFormatUnsignedShort4 = 0x09, hipResViewFormatSignedShort1 = 0x0a,
        hipResViewFormatSignedShort2 = 0x0b,
        hipResViewFormatSignedShort4 = 0x0c, hipResViewFormatUnsignedInt1 = 0x0d,
        hipResViewFormatUnsignedInt2 = 0x0e, hipResViewFormatUnsignedInt4 = 0x0f,
        hipResViewFormatSignedInt1 = 0x10,
        hipResViewFormatSignedInt2 = 0x11, hipResViewFormatSignedInt4 = 0x12,
        hipResViewFormatHalf1 = 0x13, hipResViewFormatHalf2 = 0x14,
        hipResViewFormatHalf4 = 0x15, hipResViewFormatFloat1 = 0x16,
        hipResViewFormatFloat2 = 0x17,
        hipResViewFormatFloat4 = 0x18,
        hipResViewFormatUnsignedBlockCompressed1 = 0x19,
        hipResViewFormatUnsignedBlockCompressed2 = 0x1a, hipResViewFormatUnsignedBlockCompressed3 = 0x1b,
        hipResViewFormatUnsignedBlockCompressed4 = 0x1c,
        hipResViewFormatSignedBlockCompressed4 = 0x1d,
        hipResViewFormatUnsignedBlockCompressed5 = 0x1e,
        hipResViewFormatSignedBlockCompressed5 = 0x1f,
        hipResViewFormatUnsignedBlockCompressed6H = 0x20,
        hipResViewFormatSignedBlockCompressed6H = 0x21,
        hipResViewFormatUnsignedBlockCompressed7 = 0x22
}

enum HIPresourceViewFormat_enum
{
    HIP_RES_VIEW_FORMAT_NONE = 0x00,
    HIP_RES_VIEW_FORMAT_UINT_1X8 = 0x01,
    HIP_RES_VIEW_FORMAT_UINT_2X8 = 0x02,
    HIP_RES_VIEW_FORMAT_UINT_4X8 = 0x03,
    HIP_RES_VIEW_FORMAT_SINT_1X8 = 0x04,
    HIP_RES_VIEW_FORMAT_SINT_2X8 = 0x05,
    HIP_RES_VIEW_FORMAT_SINT_4X8 = 0x06,
    HIP_RES_VIEW_FORMAT_UINT_1X16 = 0x07,
    HIP_RES_VIEW_FORMAT_UINT_2X16 = 0x08,
    HIP_RES_VIEW_FORMAT_UINT_4X16 = 0x09,
    HIP_RES_VIEW_FORMAT_SINT_1X16 = 0x0a,
    HIP_RES_VIEW_FORMAT_SINT_2X16 = 0x0b,
    HIP_RES_VIEW_FORMAT_SINT_4X16 = 0x0c,
    HIP_RES_VIEW_FORMAT_UINT_1X32 = 0x0d,
    HIP_RES_VIEW_FORMAT_UINT_2X32 = 0x0e,
    HIP_RES_VIEW_FORMAT_UINT_4X32 = 0x0f,
    HIP_RES_VIEW_FORMAT_SINT_1X32 = 0x10,
    HIP_RES_VIEW_FORMAT_SINT_2X32 = 0x11,
    HIP_RES_VIEW_FORMAT_SINT_4X32 = 0x12,
    HIP_RES_VIEW_FORMAT_FLOAT_1X16 = 0x13,
    HIP_RES_VIEW_FORMAT_FLOAT_2X16 = 0x14,
    HIP_RES_VIEW_FORMAT_FLOAT_4X16 = 0x15,
    HIP_RES_VIEW_FORMAT_FLOAT_1X32 = 0x16,
    HIP_RES_VIEW_FORMAT_FLOAT_2X32 = 0x17,
    HIP_RES_VIEW_FORMAT_FLOAT_4X32 = 0x18,
    HIP_RES_VIEW_FORMAT_UNSIGNED_BC1 = 0x19,
    HIP_RES_VIEW_FORMAT_UNSIGNED_BC2 = 0x1a,
    HIP_RES_VIEW_FORMAT_UNSIGNED_BC3 = 0x1b,
    HIP_RES_VIEW_FORMAT_UNSIGNED_BC4 = 0x1c,
    HIP_RES_VIEW_FORMAT_SIGNED_BC4 = 0x1d,
    HIP_RES_VIEW_FORMAT_UNSIGNED_BC5 = 0x1e,
    HIP_RES_VIEW_FORMAT_SIGNED_BC5 = 0x1f,
    HIP_RES_VIEW_FORMAT_UNSIGNED_BC6H = 0x20,
    HIP_RES_VIEW_FORMAT_SIGNED_BC6H = 0x21,
    HIP_RES_VIEW_FORMAT_UNSIGNED_BC7 = 0x22
}

alias HIPresourceViewFormat = HIPresourceViewFormat_enum;

struct hipResourceDesc
{
    hipResourceType resType;

    union _Anonymous_0
    {
        struct _Anonymous_1
        {
            hipArray_t array;
        }

        _Anonymous_1 array;

        struct _Anonymous_2
        {
            hipMipmappedArray_t mipmap;
        }

        _Anonymous_2 mipmap;

        struct _Anonymous_3
        {
            void* devPtr;
            hipChannelFormatDesc desc;
            size_t sizeInBytes;
        }

        _Anonymous_3 linear;

        struct _Anonymous_4
        {
            void* devPtr;
            hipChannelFormatDesc desc;
            size_t width;
            size_t height;
            size_t pitchInBytes;
        }

        _Anonymous_4 pitch2D;
    }

    _Anonymous_0 res;
}

struct HIP_RESOURCE_DESC_st
{
    HIPresourcetype resType;

    union _Anonymous_5
    {
        struct _Anonymous_6
        {
            hipArray_t hArray;
        }

        _Anonymous_6 array;

        struct _Anonymous_7
        {
            hipMipmappedArray_t hMipmappedArray;
        }

        _Anonymous_7 mipmap;

        struct _Anonymous_8
        {
            hipDeviceptr_t devPtr;
            hipArray_Format format;
            uint numChannels;
            size_t sizeInBytes;
        }

        _Anonymous_8 linear;

        struct _Anonymous_9
        {
            hipDeviceptr_t devPtr;
            hipArray_Format format;
            uint numChannels;
            size_t width;
            size_t height;
            size_t pitchInBytes;
        }

        _Anonymous_9 pitch2D;

        struct _Anonymous_10
        {
            int[32] reserved;
        }

        _Anonymous_10 reserved;
    }

    _Anonymous_5 res;
    uint flags;
}

alias HIP_RESOURCE_DESC = HIP_RESOURCE_DESC_st;

struct hipResourceViewDesc
{
    hipResourceViewFormat format;
    size_t width;
    size_t height;
    size_t depth;
    uint firstMipmapLevel;
    uint lastMipmapLevel;
    uint firstLayer;
    uint lastLayer;
}

struct HIP_RESOURCE_VIEW_DESC_st
{
    HIPresourceViewFormat format;
    size_t width;
    size_t height;
    size_t depth;
    uint firstMipmapLevel;
    uint lastMipmapLevel;
    uint firstLayer;
    uint lastLayer;
    uint[16] reserved;
}

alias HIP_RESOURCE_VIEW_DESC = HIP_RESOURCE_VIEW_DESC_st;

enum hipMemcpyKind
{
    hipMemcpyHostToHost = 0,
    hipMemcpyHostToDevice = 1,
    hipMemcpyDeviceToHost = 2,
    hipMemcpyDeviceToDevice = 3,
    hipMemcpyDefault = 4
}

struct hipPitchedPtr
{
    void* ptr;
    size_t pitch;
    size_t xsize;
    size_t ysize;
}

struct hipExtent
{
    size_t width;

    size_t height;
    size_t depth;
}

struct hipPos
{
    size_t x;
    size_t y;
    size_t z;
}

struct hipMemcpy3DParms
{
    hipArray_t srcArray;
    hipPos srcPos;
    hipPitchedPtr srcPtr;
    hipArray_t dstArray;
    hipPos dstPos;
    hipPitchedPtr dstPtr;
    hipExtent extent;
    hipMemcpyKind kind;
}

struct HIP_MEMCPY3D
{
    uint srcXInBytes;
    uint srcY;
    uint srcZ;
    uint srcLOD;
    hipMemoryType srcMemoryType;
    const(void)* srcHost;
    hipDeviceptr_t srcDevice;
    hipArray_t srcArray;
    uint srcPitch;
    uint srcHeight;
    uint dstXInBytes;
    uint dstY;
    uint dstZ;
    uint dstLOD;
    hipMemoryType dstMemoryType;
    void* dstHost;
    hipDeviceptr_t dstDevice;
    hipArray_t dstArray;
    uint dstPitch;
    uint dstHeight;
    uint WidthInBytes;
    uint Height;
    uint Depth;
}

hipPitchedPtr make_hipPitchedPtr(void* d, size_t p, size_t xsz, size_t ysz);
hipPos make_hipPos(size_t x, size_t y, size_t z);
hipExtent make_hipExtent(size_t w, size_t h, size_t d);

enum hipFunction_attribute
{
    HIP_FUNC_ATTRIBUTE_MAX_THREADS_PER_BLOCK = 0,
    HIP_FUNC_ATTRIBUTE_SHARED_SIZE_BYTES = 1,
    HIP_FUNC_ATTRIBUTE_CONST_SIZE_BYTES = 2,
    HIP_FUNC_ATTRIBUTE_LOCAL_SIZE_BYTES = 3,
    HIP_FUNC_ATTRIBUTE_NUM_REGS = 4,
    HIP_FUNC_ATTRIBUTE_PTX_VERSION = 5,
    HIP_FUNC_ATTRIBUTE_BINARY_VERSION = 6,
    HIP_FUNC_ATTRIBUTE_CACHE_MODE_CA = 7,
    HIP_FUNC_ATTRIBUTE_MAX_DYNAMIC_SHARED_SIZE_BYTES = 8,
    HIP_FUNC_ATTRIBUTE_PREFERRED_SHARED_MEMORY_CARVEOUT = 9,
    HIP_FUNC_ATTRIBUTE_MAX = 10
}

enum hipPointer_attribute
{
    HIP_POINTER_ATTRIBUTE_CONTEXT = 1,

    HIP_POINTER_ATTRIBUTE_MEMORY_TYPE = 2,
    HIP_POINTER_ATTRIBUTE_DEVICE_POINTER = 3,
    HIP_POINTER_ATTRIBUTE_HOST_POINTER = 4,
    HIP_POINTER_ATTRIBUTE_P2P_TOKENS = 5,

    HIP_POINTER_ATTRIBUTE_SYNC_MEMOPS = 6,

    HIP_POINTER_ATTRIBUTE_BUFFER_ID = 7,
    HIP_POINTER_ATTRIBUTE_IS_MANAGED = 8,
    HIP_POINTER_ATTRIBUTE_DEVICE_ORDINAL = 9,

    HIP_POINTER_ATTRIBUTE_IS_LEGACY_HIP_IPC_CAPABLE = 10,

    HIP_POINTER_ATTRIBUTE_RANGE_START_ADDR = 11,
    HIP_POINTER_ATTRIBUTE_RANGE_SIZE = 12,
    HIP_POINTER_ATTRIBUTE_MAPPED = 13,

    HIP_POINTER_ATTRIBUTE_ALLOWED_HANDLE_TYPES = 14,

    HIP_POINTER_ATTRIBUTE_IS_GPU_DIRECT_RDMA_CAPABLE = 15,

    HIP_POINTER_ATTRIBUTE_ACCESS_FLAGS = 16,

    HIP_POINTER_ATTRIBUTE_MEMPOOL_HANDLE = 17
}

hipChannelFormatDesc hipCreateChannelDesc(int x, int y, int z, int w, hipChannelFormatKind f);

struct __hip_texture;
alias hipTextureObject_t = __hip_texture*;

enum hipTextureAddressMode
{
    hipAddressModeWrap = 0,
    hipAddressModeClamp = 1,
    hipAddressModeMirror = 2,
    hipAddressModeBorder = 3
}

enum hipTextureFilterMode
{
    hipFilterModePoint = 0,
    hipFilterModeLinear = 1
}

enum hipTextureReadMode
{
    hipReadModeElementType = 0,
    hipReadModeNormalizedFloat = 1
}

struct textureReference
{
    int normalized;
    hipTextureReadMode readMode;
    hipTextureFilterMode filterMode;
    hipTextureAddressMode[3] addressMode;
    hipChannelFormatDesc channelDesc;
    int sRGB;
    uint maxAnisotropy;
    hipTextureFilterMode mipmapFilterMode;
    float mipmapLevelBias;
    float minMipmapLevelClamp;
    float maxMipmapLevelClamp;

    hipTextureObject_t textureObject;
    int numChannels;
    hipArray_Format format;
}

struct hipTextureDesc
{
    hipTextureAddressMode[3] addressMode;
    hipTextureFilterMode filterMode;
    hipTextureReadMode readMode;
    int sRGB;
    float[4] borderColor;
    int normalizedCoords;
    uint maxAnisotropy;
    hipTextureFilterMode mipmapFilterMode;
    float mipmapLevelBias;
    float minMipmapLevelClamp;
    float maxMipmapLevelClamp;
}

struct __hip_surface;
alias hipSurfaceObject_t = __hip_surface*;

struct surfaceReference
{
    hipSurfaceObject_t surfaceObject;
}

enum hipSurfaceBoundaryMode
{
    hipBoundaryModeZero = 0,
    hipBoundaryModeTrap = 1,
    hipBoundaryModeClamp = 2
}

struct ihipCtx_t;
alias hipCtx_t = ihipCtx_t*;

alias hipDevice_t = int;

enum hipDeviceP2PAttr
{
    hipDevP2PAttrPerformanceRank = 0,
    hipDevP2PAttrAccessSupported = 1,
    hipDevP2PAttrNativeAtomicSupported = 2,
    hipDevP2PAttrHipArrayAccessSupported = 3
}

struct ihipStream_t;
alias hipStream_t = ihipStream_t*;

struct hipIpcMemHandle_st
{
    char[64] reserved;
}

alias hipIpcMemHandle_t = hipIpcMemHandle_st;

struct hipIpcEventHandle_st
{
    char[64] reserved;
}

alias hipIpcEventHandle_t = hipIpcEventHandle_st;
struct ihipModule_t;
alias hipModule_t = ihipModule_t*;
struct ihipModuleSymbol_t;
alias hipFunction_t = ihipModuleSymbol_t*;

struct ihipMemPoolHandle_t;
alias hipMemPool_t = ihipMemPoolHandle_t*;

struct hipFuncAttributes
{
    int binaryVersion;
    int cacheModeCA;
    size_t constSizeBytes;
    size_t localSizeBytes;
    int maxDynamicSharedSizeBytes;
    int maxThreadsPerBlock;
    int numRegs;
    int preferredShmemCarveout;
    int ptxVersion;
    size_t sharedSizeBytes;
}

struct ihipEvent_t;
alias hipEvent_t = ihipEvent_t*;

enum hipLimit_t
{
    hipLimitPrintfFifoSize = 0x01,
    hipLimitMallocHeapSize = 0x02
}

enum hipMemoryAdvise
{
    hipMemAdviseSetReadMostly = 1,

    hipMemAdviseUnsetReadMostly = 2,
    hipMemAdviseSetPreferredLocation = 3,

    hipMemAdviseUnsetPreferredLocation = 4,
    hipMemAdviseSetAccessedBy = 5,

    hipMemAdviseUnsetAccessedBy = 6,

    hipMemAdviseSetCoarseGrain = 100,

    hipMemAdviseUnsetCoarseGrain = 101
}

enum hipMemRangeCoherencyMode
{
    hipMemRangeCoherencyModeFineGrain = 0,

    hipMemRangeCoherencyModeCoarseGrain = 1,

    hipMemRangeCoherencyModeIndeterminate = 2
}

enum hipMemRangeAttribute
{
    hipMemRangeAttributeReadMostly = 1,

    hipMemRangeAttributePreferredLocation = 2,
    hipMemRangeAttributeAccessedBy = 3,

    hipMemRangeAttributeLastPrefetchLocation = 4,

    hipMemRangeAttributeCoherencyMode = 100
}

enum hipMemPoolAttr
{
    hipMemPoolReuseFollowEventDependencies = 0x1,

    hipMemPoolReuseAllowOpportunistic = 0x2,

    hipMemPoolReuseAllowInternalDependencies = 0x3,

    hipMemPoolAttrReleaseThreshold = 0x4,

    hipMemPoolAttrReservedMemCurrent = 0x5,

    hipMemPoolAttrReservedMemHigh = 0x6,

    hipMemPoolAttrUsedMemCurrent = 0x7,

    hipMemPoolAttrUsedMemHigh = 0x8
}

enum hipMemLocationType
{
    hipMemLocationTypeInvalid = 0,
    hipMemLocationTypeDevice = 1
}

struct hipMemLocation
{
    hipMemLocationType type;
    int id;
}

enum hipMemAccessFlags
{
    hipMemAccessFlagsProtNone = 0,
    hipMemAccessFlagsProtRead = 1,
    hipMemAccessFlagsProtReadWrite = 3
}

struct hipMemAccessDesc
{
    hipMemLocation location;
    hipMemAccessFlags flags;
}

enum hipMemAllocationType
{
    hipMemAllocationTypeInvalid = 0x0,

    hipMemAllocationTypePinned = 0x1,
    hipMemAllocationTypeMax = 0x7FFFFFFF
}

enum hipMemAllocationHandleType
{
    hipMemHandleTypeNone = 0x0,
    hipMemHandleTypePosixFileDescriptor = 0x1,
    hipMemHandleTypeWin32 = 0x2,
    hipMemHandleTypeWin32Kmt = 0x4
}

struct hipMemPoolProps
{
    hipMemAllocationType allocType;
    hipMemAllocationHandleType handleTypes;
    hipMemLocation location;

    void* win32SecurityAttributes;
    ubyte[64] reserved;
}

struct hipMemPoolPtrExportData
{
    ubyte[64] reserved;
}

enum hipJitOption
{
    hipJitOptionMaxRegisters = 0,
    hipJitOptionThreadsPerBlock = 1,
    hipJitOptionWallTime = 2,
    hipJitOptionInfoLogBuffer = 3,
    hipJitOptionInfoLogBufferSizeBytes = 4,
    hipJitOptionErrorLogBuffer = 5,
    hipJitOptionErrorLogBufferSizeBytes = 6,
    hipJitOptionOptimizationLevel = 7,
    hipJitOptionTargetFromContext = 8,
    hipJitOptionTarget = 9,
    hipJitOptionFallbackStrategy = 10,
    hipJitOptionGenerateDebugInfo = 11,
    hipJitOptionLogVerbose = 12,
    hipJitOptionGenerateLineInfo = 13,
    hipJitOptionCacheMode = 14,
    hipJitOptionSm3xOpt = 15,
    hipJitOptionFastCompile = 16,
    hipJitOptionNumOptions = 17
}

enum hipFuncAttribute
{
    hipFuncAttributeMaxDynamicSharedMemorySize = 8,
    hipFuncAttributePreferredSharedMemoryCarveout = 9,
    hipFuncAttributeMax = 10
}

enum hipFuncCache_t
{
    hipFuncCachePreferNone = 0,
    hipFuncCachePreferShared = 1,
    hipFuncCachePreferL1 = 2,
    hipFuncCachePreferEqual = 3
}

enum hipSharedMemConfig
{
    hipSharedMemBankSizeDefault = 0,
    hipSharedMemBankSizeFourByte = 1,

    hipSharedMemBankSizeEightByte = 2
}

struct dim3
{
    uint x;
    uint y;
    uint z;
}

struct hipLaunchParams_t
{
    void* func;
    dim3 gridDim;
    dim3 blockDim;
    void** args;
    size_t sharedMem;
    hipStream_t stream;
}

alias hipLaunchParams = hipLaunchParams_t;

enum hipExternalMemoryHandleType_enum
{
    hipExternalMemoryHandleTypeOpaqueFd = 1,
    hipExternalMemoryHandleTypeOpaqueWin32 = 2,
    hipExternalMemoryHandleTypeOpaqueWin32Kmt = 3,
    hipExternalMemoryHandleTypeD3D12Heap = 4,
    hipExternalMemoryHandleTypeD3D12Resource = 5,
    hipExternalMemoryHandleTypeD3D11Resource = 6,
    hipExternalMemoryHandleTypeD3D11ResourceKmt = 7
}

alias hipExternalMemoryHandleType = hipExternalMemoryHandleType_enum;

struct hipExternalMemoryHandleDesc_st
{
    hipExternalMemoryHandleType type;

    union _Anonymous_11
    {
        int fd;

        struct _Anonymous_12
        {
            void* handle;
            const(void)* name;
        }

        _Anonymous_12 win32;
    }

    _Anonymous_11 handle;
    ulong size;
    uint flags;
}

alias hipExternalMemoryHandleDesc = hipExternalMemoryHandleDesc_st;

struct hipExternalMemoryBufferDesc_st
{
    ulong offset;
    ulong size;
    uint flags;
}

alias hipExternalMemoryBufferDesc = hipExternalMemoryBufferDesc_st;
alias hipExternalMemory_t = void*;

enum hipExternalSemaphoreHandleType_enum
{
    hipExternalSemaphoreHandleTypeOpaqueFd = 1,
    hipExternalSemaphoreHandleTypeOpaqueWin32 = 2,
    hipExternalSemaphoreHandleTypeOpaqueWin32Kmt = 3,
    hipExternalSemaphoreHandleTypeD3D12Fence = 4
}

alias hipExternalSemaphoreHandleType = hipExternalSemaphoreHandleType_enum;

struct hipExternalSemaphoreHandleDesc_st
{
    hipExternalSemaphoreHandleType type;

    union _Anonymous_13
    {
        int fd;

        struct _Anonymous_14
        {
            void* handle;
            const(void)* name;
        }

        _Anonymous_14 win32;
    }

    _Anonymous_13 handle;
    uint flags;
}

alias hipExternalSemaphoreHandleDesc = hipExternalSemaphoreHandleDesc_st;
alias hipExternalSemaphore_t = void*;

struct hipExternalSemaphoreSignalParams_st
{
    struct _Anonymous_15
    {
        struct _Anonymous_16
        {
            ulong value;
        }

        _Anonymous_16 fence;

        struct _Anonymous_17
        {
            ulong key;
        }

        _Anonymous_17 keyedMutex;
        uint[12] reserved;
    }

    _Anonymous_15 params;
    uint flags;
    uint[16] reserved;
}

alias hipExternalSemaphoreSignalParams = hipExternalSemaphoreSignalParams_st;

struct hipExternalSemaphoreWaitParams_st
{
    struct _Anonymous_18
    {
        struct _Anonymous_19
        {
            ulong value;
        }

        _Anonymous_19 fence;

        struct _Anonymous_20
        {
            ulong key;
            uint timeoutMs;
        }

        _Anonymous_20 keyedMutex;
        uint[10] reserved;
    }

    _Anonymous_18 params;
    uint flags;
    uint[16] reserved;
}

alias hipExternalSemaphoreWaitParams = hipExternalSemaphoreWaitParams_st;

void __hipGetPCH(const(char*)* pch, uint* size);

enum hipGLDeviceList
{
    hipGLDeviceListAll = 1,
    hipGLDeviceListCurrentFrame = 2,

    hipGLDeviceListNextFrame = 3
}

enum hipGraphicsRegisterFlags
{
    hipGraphicsRegisterFlagsNone = 0,
    hipGraphicsRegisterFlagsReadOnly = 1,
    hipGraphicsRegisterFlagsWriteDiscard = 2,
    hipGraphicsRegisterFlagsSurfaceLoadStore = 4,
    hipGraphicsRegisterFlagsTextureGather = 8
}

struct _hipGraphicsResource;
alias hipGraphicsResource = _hipGraphicsResource;

alias hipGraphicsResource_t = _hipGraphicsResource*;

struct ihipGraph;
alias hipGraph_t = ihipGraph*;

struct hipGraphNode;
alias hipGraphNode_t = hipGraphNode*;

struct hipGraphExec;
alias hipGraphExec_t = hipGraphExec*;

enum hipGraphNodeType
{
    hipGraphNodeTypeKernel = 1,
    hipGraphNodeTypeMemcpy = 2,
    hipGraphNodeTypeMemset = 3,
    hipGraphNodeTypeHost = 4,
    hipGraphNodeTypeGraph = 5,
    hipGraphNodeTypeEmpty = 6,
    hipGraphNodeTypeWaitEvent = 7,
    hipGraphNodeTypeEventRecord = 8,
    hipGraphNodeTypeMemcpy1D = 9,
    hipGraphNodeTypeMemcpyFromSymbol = 10,
    hipGraphNodeTypeMemcpyToSymbol = 11,
    hipGraphNodeTypeCount = 12
}

alias hipHostFn_t = void function(void* userData);

struct hipHostNodeParams
{
    hipHostFn_t fn;
    void* userData;
}

struct hipKernelNodeParams
{
    dim3 blockDim;
    void** extra;
    void* func;
    dim3 gridDim;
    void** kernelParams;
    uint sharedMemBytes;
}

struct hipMemsetParams
{
    void* dst;
    uint elementSize;
    size_t height;
    size_t pitch;
    uint value;
    size_t width;
}

enum hipGraphExecUpdateResult
{
    hipGraphExecUpdateSuccess = 0x0,
    hipGraphExecUpdateError = 0x1,

    hipGraphExecUpdateErrorTopologyChanged = 0x2,
    hipGraphExecUpdateErrorNodeTypeChanged = 0x3,
    hipGraphExecUpdateErrorFunctionChanged = 0x4,
    hipGraphExecUpdateErrorParametersChanged = 0x5,
    hipGraphExecUpdateErrorNotSupported = 0x6,
    hipGraphExecUpdateErrorUnsupportedFunctionChange = 0x7
}

enum hipStreamCaptureMode
{
    hipStreamCaptureModeGlobal = 0,
    hipStreamCaptureModeThreadLocal = 1,
    hipStreamCaptureModeRelaxed = 2
}

enum hipStreamCaptureStatus
{
    hipStreamCaptureStatusNone = 0,
    hipStreamCaptureStatusActive = 1,
    hipStreamCaptureStatusInvalidated = 2
}

enum hipStreamUpdateCaptureDependenciesFlags
{
    hipStreamAddCaptureDependencies = 0,
    hipStreamSetCaptureDependencies = 1
}

enum hipGraphInstantiateFlags
{
    hipGraphInstantiateFlagAutoFreeOnLaunch = 1
}

hipError_t hipMemcpy_spt(void* dst, const(void)* src, size_t sizeBytes, hipMemcpyKind kind);

hipError_t hipMemcpyToSymbol_spt(const(void)* symbol, const(void)* src,
        size_t sizeBytes, size_t offset, hipMemcpyKind kind);

hipError_t hipMemcpyFromSymbol_spt(void* dst, const(void)* symbol,
        size_t sizeBytes, size_t offset, hipMemcpyKind kind);

hipError_t hipMemcpy2D_spt(void* dst, size_t dpitch, const(void)* src,
        size_t spitch, size_t width, size_t height, hipMemcpyKind kind);

hipError_t hipMemcpy2DToArray_spt(hipArray* dst, size_t wOffset, size_t hOffset,
        const(void)* src, size_t spitch, size_t width, size_t height, hipMemcpyKind kind);

hipError_t hipMemcpy2DFromArray_spt(void* dst, size_t dpitch, hipArray_const_t src,
        size_t wOffset, size_t hOffset, size_t width, size_t height, hipMemcpyKind kind);

hipError_t hipMemcpy3D_spt(const(hipMemcpy3DParms)* p);

hipError_t hipMemset_spt(void* dst, int value, size_t sizeBytes);

hipError_t hipMemset2D_spt(void* dst, size_t pitch, int value, size_t width, size_t height);

hipError_t hipMemset3D_spt(hipPitchedPtr pitchedDevPtr, int value, hipExtent extent);

hipError_t hipMemcpyAsync_spt(void* dst, const(void)* src, size_t sizeBytes,
        hipMemcpyKind kind, hipStream_t stream);

hipError_t hipStreamQuery_spt(hipStream_t stream);

hipError_t hipStreamSynchronize_spt(hipStream_t stream);

hipError_t hipStreamGetPriority_spt(hipStream_t stream, int* priority);

hipError_t hipStreamWaitEvent_spt(hipStream_t stream, hipEvent_t event, uint flags);

hipError_t hipStreamGetFlags_spt(hipStream_t stream, uint* flags);

hipError_t hipLaunchCooperativeKernel_spt(const(void)* f, dim3 gridDim,
        dim3 blockDim, void** kernelParams, uint sharedMemBytes, hipStream_t hStream);

hipError_t hipLaunchKernel_spt(const(void)* function_address, dim3 numBlocks,
        dim3 dimBlocks, void** args, size_t sharedMemBytes, hipStream_t stream);

hipError_t hipInit(uint flags);

hipError_t hipDriverGetVersion(int* driverVersion);

hipError_t hipRuntimeGetVersion(int* runtimeVersion);

hipError_t hipDeviceGet(hipDevice_t* device, int ordinal);

hipError_t hipDeviceComputeCapability(int* major, int* minor, hipDevice_t device);

hipError_t hipDeviceGetName(char* name, int len, hipDevice_t device);

hipError_t hipDeviceGetUuid(hipUUID* uuid, hipDevice_t device);

hipError_t hipDeviceGetP2PAttribute(int* value, hipDeviceP2PAttr attr, int srcDevice, int dstDevice);

hipError_t hipDeviceGetPCIBusId(char* pciBusId, int len, int device);

hipError_t hipDeviceGetByPCIBusId(int* device, const(char)* pciBusId);

hipError_t hipDeviceTotalMem(size_t* bytes, hipDevice_t device);

hipError_t hipDeviceSynchronize();

hipError_t hipDeviceReset();

hipError_t hipSetDevice(int deviceId);

hipError_t hipGetDevice(int* deviceId);

hipError_t hipGetDeviceCount(int* count);

hipError_t hipDeviceGetAttribute(int* pi, hipDeviceAttribute_t attr, int deviceId);

hipError_t hipDeviceGetDefaultMemPool(hipMemPool_t* mem_pool, int device);

hipError_t hipDeviceSetMemPool(int device, hipMemPool_t mem_pool);

hipError_t hipDeviceGetMemPool(hipMemPool_t* mem_pool, int device);

hipError_t hipGetDeviceProperties(hipDeviceProp_t* prop, int deviceId);

hipError_t hipDeviceSetCacheConfig(hipFuncCache_t cacheConfig);

hipError_t hipDeviceGetCacheConfig(hipFuncCache_t* cacheConfig);

hipError_t hipDeviceGetLimit(size_t* pValue, hipLimit_t limit);

hipError_t hipDeviceGetSharedMemConfig(hipSharedMemConfig* pConfig);

hipError_t hipGetDeviceFlags(uint* flags);

hipError_t hipDeviceSetSharedMemConfig(hipSharedMemConfig config);

hipError_t hipSetDeviceFlags(uint flags);

hipError_t hipChooseDevice(int* device, const(hipDeviceProp_t)* prop);

hipError_t hipExtGetLinkTypeAndHopCount(int device1, int device2, uint* linktype, uint* hopcount);

hipError_t hipIpcGetMemHandle(hipIpcMemHandle_t* handle, void* devPtr);

hipError_t hipIpcOpenMemHandle(void** devPtr, hipIpcMemHandle_t handle, uint flags);

hipError_t hipIpcCloseMemHandle(void* devPtr);

hipError_t hipIpcGetEventHandle(hipIpcEventHandle_t* handle, hipEvent_t event);

hipError_t hipIpcOpenEventHandle(hipEvent_t* event, hipIpcEventHandle_t handle);

hipError_t hipFuncSetAttribute(const(void)* func, hipFuncAttribute attr, int value);

hipError_t hipFuncSetCacheConfig(const(void)* func, hipFuncCache_t config);

hipError_t hipFuncSetSharedMemConfig(const(void)* func, hipSharedMemConfig config);

hipError_t hipGetLastError();

hipError_t hipPeekAtLastError();

const(char)* hipGetErrorName(hipError_t hip_error);

const(char)* hipGetErrorString(hipError_t hipError);

hipError_t hipStreamCreate(hipStream_t* stream);

hipError_t hipStreamCreateWithFlags(hipStream_t* stream, uint flags);

hipError_t hipStreamCreateWithPriority(hipStream_t* stream, uint flags, int priority);

hipError_t hipDeviceGetStreamPriorityRange(int* leastPriority, int* greatestPriority);

hipError_t hipStreamDestroy(hipStream_t stream);

hipError_t hipStreamQuery(hipStream_t stream);

hipError_t hipStreamSynchronize(hipStream_t stream);

hipError_t hipStreamWaitEvent(hipStream_t stream, hipEvent_t event, uint flags);

hipError_t hipStreamGetFlags(hipStream_t stream, uint* flags);

hipError_t hipStreamGetPriority(hipStream_t stream, int* priority);

hipError_t hipExtStreamCreateWithCUMask(hipStream_t* stream, uint cuMaskSize, const(uint)* cuMask);

hipError_t hipExtStreamGetCUMask(hipStream_t stream, uint cuMaskSize, uint* cuMask);

alias hipStreamCallback_t = void function(hipStream_t stream, hipError_t status, void* userData);

hipError_t hipStreamAddCallback(hipStream_t stream, hipStreamCallback_t callback,
        void* userData, uint flags);

hipError_t hipStreamWaitValue32(hipStream_t stream, void* ptr, uint value, uint flags, uint mask);

hipError_t hipStreamWaitValue64(hipStream_t stream, void* ptr, ulong value, uint flags, ulong mask);

hipError_t hipStreamWriteValue32(hipStream_t stream, void* ptr, uint value, uint flags);

hipError_t hipStreamWriteValue64(hipStream_t stream, void* ptr, ulong value, uint flags);

hipError_t hipEventCreateWithFlags(hipEvent_t* event, uint flags);

hipError_t hipEventCreate(hipEvent_t* event);

hipError_t hipEventRecord(hipEvent_t event, hipStream_t stream);

hipError_t hipEventDestroy(hipEvent_t event);

hipError_t hipEventSynchronize(hipEvent_t event);

hipError_t hipEventElapsedTime(float* ms, hipEvent_t start, hipEvent_t stop);

hipError_t hipEventQuery(hipEvent_t event);

hipError_t hipPointerGetAttributes(hipPointerAttribute_t* attributes, const(void)* ptr);

hipError_t hipPointerGetAttribute(void* data, hipPointer_attribute attribute, hipDeviceptr_t ptr);

hipError_t hipDrvPointerGetAttributes(uint numAttributes,
        hipPointer_attribute* attributes, void** data, hipDeviceptr_t ptr);

hipError_t hipImportExternalSemaphore(hipExternalSemaphore_t* extSem_out,
        const(hipExternalSemaphoreHandleDesc)* semHandleDesc);

hipError_t hipSignalExternalSemaphoresAsync(const(hipExternalSemaphore_t)* extSemArray,
        const(hipExternalSemaphoreSignalParams)* paramsArray, uint numExtSems, hipStream_t stream);

hipError_t hipWaitExternalSemaphoresAsync(const(hipExternalSemaphore_t)* extSemArray,
        const(hipExternalSemaphoreWaitParams)* paramsArray, uint numExtSems, hipStream_t stream);

hipError_t hipDestroyExternalSemaphore(hipExternalSemaphore_t extSem);

hipError_t hipImportExternalMemory(hipExternalMemory_t* extMem_out,
        const(hipExternalMemoryHandleDesc)* memHandleDesc);

hipError_t hipExternalMemoryGetMappedBuffer(void** devPtr,
        hipExternalMemory_t extMem, const(hipExternalMemoryBufferDesc)* bufferDesc);

hipError_t hipDestroyExternalMemory(hipExternalMemory_t extMem);

hipError_t hipMalloc(void** ptr, size_t size);

hipError_t hipExtMallocWithFlags(void** ptr, size_t sizeBytes, uint flags);

hipError_t hipMallocHost(void** ptr, size_t size);

hipError_t hipMemAllocHost(void** ptr, size_t size);

hipError_t hipHostMalloc(void** ptr, size_t size, uint flags);

hipError_t hipMallocManaged(void** dev_ptr, size_t size, uint flags);

hipError_t hipMemPrefetchAsync(const(void)* dev_ptr, size_t count, int device, hipStream_t stream);

hipError_t hipMemAdvise(const(void)* dev_ptr, size_t count, hipMemoryAdvise advice, int device);

hipError_t hipMemRangeGetAttribute(void* data, size_t data_size,
        hipMemRangeAttribute attribute, const(void)* dev_ptr, size_t count);

hipError_t hipMemRangeGetAttributes(void** data, size_t* data_sizes,
        hipMemRangeAttribute* attributes, size_t num_attributes, const(void)* dev_ptr, size_t count);

hipError_t hipStreamAttachMemAsync(hipStream_t stream, void* dev_ptr, size_t length, uint flags);

hipError_t hipMallocAsync(void** dev_ptr, size_t size, hipStream_t stream);

hipError_t hipFreeAsync(void* dev_ptr, hipStream_t stream);

hipError_t hipMemPoolTrimTo(hipMemPool_t mem_pool, size_t min_bytes_to_hold);

hipError_t hipMemPoolSetAttribute(hipMemPool_t mem_pool, hipMemPoolAttr attr, void* value);

hipError_t hipMemPoolGetAttribute(hipMemPool_t mem_pool, hipMemPoolAttr attr, void* value);

hipError_t hipMemPoolSetAccess(hipMemPool_t mem_pool,
        const(hipMemAccessDesc)* desc_list, size_t count);

hipError_t hipMemPoolGetAccess(hipMemAccessFlags* flags, hipMemPool_t mem_pool,
        hipMemLocation* location);

hipError_t hipMemPoolCreate(hipMemPool_t* mem_pool, const(hipMemPoolProps)* pool_props);

hipError_t hipMemPoolDestroy(hipMemPool_t mem_pool);

hipError_t hipMallocFromPoolAsync(void** dev_ptr, size_t size,
        hipMemPool_t mem_pool, hipStream_t stream);

hipError_t hipMemPoolExportToShareableHandle(void* shared_handle,
        hipMemPool_t mem_pool, hipMemAllocationHandleType handle_type, uint flags);

hipError_t hipMemPoolImportFromShareableHandle(hipMemPool_t* mem_pool,
        void* shared_handle, hipMemAllocationHandleType handle_type, uint flags);

hipError_t hipMemPoolExportPointer(hipMemPoolPtrExportData* export_data, void* dev_ptr);

hipError_t hipMemPoolImportPointer(void** dev_ptr, hipMemPool_t mem_pool,
        hipMemPoolPtrExportData* export_data);

hipError_t hipHostAlloc(void** ptr, size_t size, uint flags);

hipError_t hipHostGetDevicePointer(void** devPtr, void* hstPtr, uint flags);

hipError_t hipHostGetFlags(uint* flagsPtr, void* hostPtr);

hipError_t hipHostRegister(void* hostPtr, size_t sizeBytes, uint flags);

hipError_t hipHostUnregister(void* hostPtr);

hipError_t hipMallocPitch(void** ptr, size_t* pitch, size_t width, size_t height);

hipError_t hipMemAllocPitch(hipDeviceptr_t* dptr, size_t* pitch,
        size_t widthInBytes, size_t height, uint elementSizeBytes);

hipError_t hipFree(void* ptr);

hipError_t hipFreeHost(void* ptr);

hipError_t hipHostFree(void* ptr);

hipError_t hipMemcpy(void* dst, const(void)* src, size_t sizeBytes, hipMemcpyKind kind);

hipError_t hipMemcpyWithStream(void* dst, const(void)* src, size_t sizeBytes,
        hipMemcpyKind kind, hipStream_t stream);

hipError_t hipMemcpyHtoD(hipDeviceptr_t dst, void* src, size_t sizeBytes);

hipError_t hipMemcpyDtoH(void* dst, hipDeviceptr_t src, size_t sizeBytes);

hipError_t hipMemcpyDtoD(hipDeviceptr_t dst, hipDeviceptr_t src, size_t sizeBytes);

hipError_t hipMemcpyHtoDAsync(hipDeviceptr_t dst, void* src, size_t sizeBytes, hipStream_t stream);

hipError_t hipMemcpyDtoHAsync(void* dst, hipDeviceptr_t src, size_t sizeBytes, hipStream_t stream);

hipError_t hipMemcpyDtoDAsync(hipDeviceptr_t dst, hipDeviceptr_t src,
        size_t sizeBytes, hipStream_t stream);

hipError_t hipModuleGetGlobal(hipDeviceptr_t* dptr, size_t* bytes,
        hipModule_t hmod, const(char)* name);

hipError_t hipGetSymbolAddress(void** devPtr, const(void)* symbol);

hipError_t hipGetSymbolSize(size_t* size, const(void)* symbol);

hipError_t hipMemcpyToSymbol(const(void)* symbol, const(void)* src,
        size_t sizeBytes, size_t offset, hipMemcpyKind kind);

hipError_t hipMemcpyToSymbolAsync(const(void)* symbol, const(void)* src,
        size_t sizeBytes, size_t offset, hipMemcpyKind kind, hipStream_t stream);

hipError_t hipMemcpyFromSymbol(void* dst, const(void)* symbol, size_t sizeBytes,
        size_t offset, hipMemcpyKind kind);

hipError_t hipMemcpyFromSymbolAsync(void* dst, const(void)* symbol,
        size_t sizeBytes, size_t offset, hipMemcpyKind kind, hipStream_t stream);

hipError_t hipMemcpyAsync(void* dst, const(void)* src, size_t sizeBytes,
        hipMemcpyKind kind, hipStream_t stream);

hipError_t hipMemset(void* dst, int value, size_t sizeBytes);

hipError_t hipMemsetD8(hipDeviceptr_t dest, ubyte value, size_t count);

hipError_t hipMemsetD8Async(hipDeviceptr_t dest, ubyte value, size_t count, hipStream_t stream);

hipError_t hipMemsetD16(hipDeviceptr_t dest, ushort value, size_t count);

hipError_t hipMemsetD16Async(hipDeviceptr_t dest, ushort value, size_t count, hipStream_t stream);

hipError_t hipMemsetD32(hipDeviceptr_t dest, int value, size_t count);

hipError_t hipMemsetAsync(void* dst, int value, size_t sizeBytes, hipStream_t stream);

hipError_t hipMemsetD32Async(hipDeviceptr_t dst, int value, size_t count, hipStream_t stream);

hipError_t hipMemset2D(void* dst, size_t pitch, int value, size_t width, size_t height);

hipError_t hipMemset2DAsync(void* dst, size_t pitch, int value, size_t width,
        size_t height, hipStream_t stream);

hipError_t hipMemset3D(hipPitchedPtr pitchedDevPtr, int value, hipExtent extent);

hipError_t hipMemset3DAsync(hipPitchedPtr pitchedDevPtr, int value,
        hipExtent extent, hipStream_t stream);

hipError_t hipMemGetInfo(size_t* free, size_t* total);
hipError_t hipMemPtrGetInfo(void* ptr, size_t* size);

hipError_t hipMallocArray(hipArray** array, const(hipChannelFormatDesc)* desc,
        size_t width, size_t height, uint flags);
hipError_t hipArrayCreate(hipArray** pHandle, const(HIP_ARRAY_DESCRIPTOR)* pAllocateArray);
hipError_t hipArrayDestroy(hipArray* array);
hipError_t hipArray3DCreate(hipArray** array, const(HIP_ARRAY3D_DESCRIPTOR)* pAllocateArray);
hipError_t hipMalloc3D(hipPitchedPtr* pitchedDevPtr, hipExtent extent);

hipError_t hipFreeArray(hipArray* array);

hipError_t hipFreeMipmappedArray(hipMipmappedArray_t mipmappedArray);

hipError_t hipMalloc3DArray(hipArray** array,
        const(hipChannelFormatDesc)* desc, hipExtent extent, uint flags);

hipError_t hipMallocMipmappedArray(hipMipmappedArray_t* mipmappedArray,
        const(hipChannelFormatDesc)* desc, hipExtent extent, uint numLevels, uint flags);

hipError_t hipGetMipmappedArrayLevel(hipArray_t* levelArray,
        hipMipmappedArray_const_t mipmappedArray, uint level);

hipError_t hipMemcpy2D(void* dst, size_t dpitch, const(void)* src, size_t spitch,
        size_t width, size_t height, hipMemcpyKind kind);

hipError_t hipMemcpyParam2D(const(hip_Memcpy2D)* pCopy);

hipError_t hipMemcpyParam2DAsync(const(hip_Memcpy2D)* pCopy, hipStream_t stream);

hipError_t hipMemcpy2DAsync(void* dst, size_t dpitch, const(void)* src,
        size_t spitch, size_t width, size_t height, hipMemcpyKind kind, hipStream_t stream);

hipError_t hipMemcpy2DToArray(hipArray* dst, size_t wOffset, size_t hOffset,
        const(void)* src, size_t spitch, size_t width, size_t height, hipMemcpyKind kind);

hipError_t hipMemcpy2DToArrayAsync(hipArray* dst, size_t wOffset, size_t hOffset, const(void)* src,
        size_t spitch, size_t width, size_t height, hipMemcpyKind kind, hipStream_t stream);

hipError_t hipMemcpyToArray(hipArray* dst, size_t wOffset, size_t hOffset,
        const(void)* src, size_t count, hipMemcpyKind kind);

hipError_t hipMemcpyFromArray(void* dst, hipArray_const_t srcArray,
        size_t wOffset, size_t hOffset, size_t count, hipMemcpyKind kind);

hipError_t hipMemcpy2DFromArray(void* dst, size_t dpitch, hipArray_const_t src,
        size_t wOffset, size_t hOffset, size_t width, size_t height, hipMemcpyKind kind);

hipError_t hipMemcpy2DFromArrayAsync(void* dst, size_t dpitch, hipArray_const_t src, size_t wOffset,
        size_t hOffset, size_t width, size_t height, hipMemcpyKind kind, hipStream_t stream);

hipError_t hipMemcpyAtoH(void* dst, hipArray* srcArray, size_t srcOffset, size_t count);

hipError_t hipMemcpyHtoA(hipArray* dstArray, size_t dstOffset, const(void)* srcHost, size_t count);

hipError_t hipMemcpy3D(const(hipMemcpy3DParms)* p);

hipError_t hipMemcpy3DAsync(const(hipMemcpy3DParms)* p, hipStream_t stream);

hipError_t hipDrvMemcpy3D(const(HIP_MEMCPY3D)* pCopy);

hipError_t hipDrvMemcpy3DAsync(const(HIP_MEMCPY3D)* pCopy, hipStream_t stream);

hipError_t hipDeviceCanAccessPeer(int* canAccessPeer, int deviceId, int peerDeviceId);

hipError_t hipDeviceEnablePeerAccess(int peerDeviceId, uint flags);

hipError_t hipDeviceDisablePeerAccess(int peerDeviceId);

hipError_t hipMemGetAddressRange(hipDeviceptr_t* pbase, size_t* psize, hipDeviceptr_t dptr);

hipError_t hipMemcpyPeer(void* dst, int dstDeviceId, const(void)* src,
        int srcDeviceId, size_t sizeBytes);

hipError_t hipMemcpyPeerAsync(void* dst, int dstDeviceId, const(void)* src,
        int srcDevice, size_t sizeBytes, hipStream_t stream);

hipError_t hipCtxCreate(hipCtx_t* ctx, uint flags, hipDevice_t device);

hipError_t hipCtxDestroy(hipCtx_t ctx);

hipError_t hipCtxPopCurrent(hipCtx_t* ctx);

hipError_t hipCtxPushCurrent(hipCtx_t ctx);

hipError_t hipCtxSetCurrent(hipCtx_t ctx);

hipError_t hipCtxGetCurrent(hipCtx_t* ctx);

hipError_t hipCtxGetDevice(hipDevice_t* device);

hipError_t hipCtxGetApiVersion(hipCtx_t ctx, int* apiVersion);

hipError_t hipCtxGetCacheConfig(hipFuncCache_t* cacheConfig);

hipError_t hipCtxSetCacheConfig(hipFuncCache_t cacheConfig);

hipError_t hipCtxSetSharedMemConfig(hipSharedMemConfig config);

hipError_t hipCtxGetSharedMemConfig(hipSharedMemConfig* pConfig);

hipError_t hipCtxSynchronize();

hipError_t hipCtxGetFlags(uint* flags);

hipError_t hipCtxEnablePeerAccess(hipCtx_t peerCtx, uint flags);

hipError_t hipCtxDisablePeerAccess(hipCtx_t peerCtx);

hipError_t hipDevicePrimaryCtxGetState(hipDevice_t dev, uint* flags, int* active);

hipError_t hipDevicePrimaryCtxRelease(hipDevice_t dev);

hipError_t hipDevicePrimaryCtxRetain(hipCtx_t* pctx, hipDevice_t dev);

hipError_t hipDevicePrimaryCtxReset(hipDevice_t dev);

hipError_t hipDevicePrimaryCtxSetFlags(hipDevice_t dev, uint flags);

hipError_t hipModuleLoad(hipModule_t* module_, const(char)* fname);

hipError_t hipModuleUnload(hipModule_t module_);

hipError_t hipModuleGetFunction(hipFunction_t* function_, hipModule_t module_, const(char)* kname);

hipError_t hipFuncGetAttributes(hipFuncAttributes* attr, const(void)* func);

hipError_t hipFuncGetAttribute(int* value, hipFunction_attribute attrib, hipFunction_t hfunc);

hipError_t hipModuleGetTexRef(textureReference** texRef, hipModule_t hmod, const(char)* name);

hipError_t hipModuleLoadData(hipModule_t* module_, const(void)* image);

hipError_t hipModuleLoadDataEx(hipModule_t* module_, const(void)* image,
        uint numOptions, hipJitOption* options, void** optionValues);

hipError_t hipModuleLaunchKernel(hipFunction_t f, uint gridDimX, uint gridDimY, uint gridDimZ, uint blockDimX,
        uint blockDimY, uint blockDimZ, uint sharedMemBytes, hipStream_t stream,
        void** kernelParams, void** extra);

hipError_t hipLaunchCooperativeKernel(const(void)* f, dim3 gridDim,
        dim3 blockDimX, void** kernelParams, uint sharedMemBytes, hipStream_t stream);

hipError_t hipLaunchCooperativeKernelMultiDevice(hipLaunchParams* launchParamsList,
        int numDevices, uint flags);

hipError_t hipExtLaunchMultiKernelMultiDevice(hipLaunchParams* launchParamsList,
        int numDevices, uint flags);

hipError_t hipModuleOccupancyMaxPotentialBlockSize(int* gridSize,
        int* blockSize, hipFunction_t f, size_t dynSharedMemPerBlk, int blockSizeLimit);

hipError_t hipModuleOccupancyMaxPotentialBlockSizeWithFlags(int* gridSize,
        int* blockSize, hipFunction_t f, size_t dynSharedMemPerBlk, int blockSizeLimit, uint flags);

hipError_t hipModuleOccupancyMaxActiveBlocksPerMultiprocessor(int* numBlocks,
        hipFunction_t f, int blockSize, size_t dynSharedMemPerBlk);

hipError_t hipModuleOccupancyMaxActiveBlocksPerMultiprocessorWithFlags(int* numBlocks,
        hipFunction_t f, int blockSize, size_t dynSharedMemPerBlk, uint flags);

hipError_t hipOccupancyMaxActiveBlocksPerMultiprocessor(int* numBlocks,
        const(void)* f, int blockSize, size_t dynSharedMemPerBlk);

hipError_t hipOccupancyMaxActiveBlocksPerMultiprocessorWithFlags(int* numBlocks,
        const(void)* f, int blockSize, size_t dynSharedMemPerBlk, uint flags);

hipError_t hipOccupancyMaxPotentialBlockSize(int* gridSize, int* blockSize,
        const(void)* f, size_t dynSharedMemPerBlk, int blockSizeLimit);

hipError_t hipProfilerStart();

hipError_t hipProfilerStop();

hipError_t hipConfigureCall(dim3 gridDim, dim3 blockDim, size_t sharedMem, hipStream_t stream);

hipError_t hipSetupArgument(const(void)* arg, size_t size, size_t offset);

hipError_t hipLaunchByPtr(const(void)* func);

hipError_t __hipPushCallConfiguration(dim3 gridDim, dim3 blockDim,
        size_t sharedMem, hipStream_t stream);

hipError_t __hipPopCallConfiguration(dim3* gridDim, dim3* blockDim,
        size_t* sharedMem, hipStream_t* stream);

hipError_t hipLaunchKernel(const(void)* function_address, dim3 numBlocks,
        dim3 dimBlocks, void** args, size_t sharedMemBytes, hipStream_t stream);

hipError_t hipDrvMemcpy2DUnaligned(const(hip_Memcpy2D)* pCopy);

hipError_t hipExtLaunchKernel(const(void)* function_address, dim3 numBlocks, dim3 dimBlocks, void** args,
        size_t sharedMemBytes, hipStream_t stream, hipEvent_t startEvent,
        hipEvent_t stopEvent, int flags);

hipError_t hipBindTextureToMipmappedArray(const(textureReference)* tex,
        hipMipmappedArray_const_t mipmappedArray, const(hipChannelFormatDesc)* desc);

hipError_t hipGetTextureReference(const(textureReference*)* texref, const(void)* symbol);

hipError_t hipCreateTextureObject(hipTextureObject_t* pTexObject, const(hipResourceDesc)* pResDesc,
        const(hipTextureDesc)* pTexDesc, const(hipResourceViewDesc)* pResViewDesc);

hipError_t hipDestroyTextureObject(hipTextureObject_t textureObject);

hipError_t hipGetChannelDesc(hipChannelFormatDesc* desc, hipArray_const_t array);

hipError_t hipGetTextureObjectResourceDesc(hipResourceDesc* pResDesc,
        hipTextureObject_t textureObject);

hipError_t hipGetTextureObjectResourceViewDesc(hipResourceViewDesc* pResViewDesc,
        hipTextureObject_t textureObject);

hipError_t hipGetTextureObjectTextureDesc(hipTextureDesc* pTexDesc, hipTextureObject_t textureObject);

hipError_t hipTexRefSetAddressMode(textureReference* texRef, int dim, hipTextureAddressMode am);
hipError_t hipTexRefSetArray(textureReference* tex, hipArray_const_t array, uint flags);
hipError_t hipTexRefSetFilterMode(textureReference* texRef, hipTextureFilterMode fm);
hipError_t hipTexRefSetFlags(textureReference* texRef, uint Flags);
hipError_t hipTexRefSetFormat(textureReference* texRef, hipArray_Format fmt,
        int NumPackedComponents);
hipError_t hipTexObjectCreate(hipTextureObject_t* pTexObject, const(HIP_RESOURCE_DESC)* pResDesc,
        const(HIP_TEXTURE_DESC)* pTexDesc, const(HIP_RESOURCE_VIEW_DESC)* pResViewDesc);
hipError_t hipTexObjectDestroy(hipTextureObject_t texObject);
hipError_t hipTexObjectGetResourceDesc(HIP_RESOURCE_DESC* pResDesc, hipTextureObject_t texObject);
hipError_t hipTexObjectGetResourceViewDesc(HIP_RESOURCE_VIEW_DESC* pResViewDesc,
        hipTextureObject_t texObject);
hipError_t hipTexObjectGetTextureDesc(HIP_TEXTURE_DESC* pTexDesc, hipTextureObject_t texObject);

hipError_t hipBindTexture(size_t* offset, const(textureReference)* tex,
        const(void)* devPtr, const(hipChannelFormatDesc)* desc, size_t size);
hipError_t hipBindTexture2D(size_t* offset, const(textureReference)* tex,
        const(void)* devPtr, const(hipChannelFormatDesc)* desc, size_t width,
        size_t height, size_t pitch);
hipError_t hipBindTextureToArray(const(textureReference)* tex,
        hipArray_const_t array, const(hipChannelFormatDesc)* desc);
hipError_t hipGetTextureAlignmentOffset(size_t* offset, const(textureReference)* texref);
hipError_t hipUnbindTexture(const(textureReference)* tex);
hipError_t hipTexRefGetAddress(hipDeviceptr_t* dev_ptr, const(textureReference)* texRef);
hipError_t hipTexRefGetAddressMode(hipTextureAddressMode* pam,
        const(textureReference)* texRef, int dim);
hipError_t hipTexRefGetFilterMode(hipTextureFilterMode* pfm, const(textureReference)* texRef);
hipError_t hipTexRefGetFlags(uint* pFlags, const(textureReference)* texRef);
hipError_t hipTexRefGetFormat(hipArray_Format* pFormat, int* pNumChannels,
        const(textureReference)* texRef);
hipError_t hipTexRefGetMaxAnisotropy(int* pmaxAnsio, const(textureReference)* texRef);
hipError_t hipTexRefGetMipmapFilterMode(hipTextureFilterMode* pfm, const(textureReference)* texRef);
hipError_t hipTexRefGetMipmapLevelBias(float* pbias, const(textureReference)* texRef);
hipError_t hipTexRefGetMipmapLevelClamp(float* pminMipmapLevelClamp,
        float* pmaxMipmapLevelClamp, const(textureReference)* texRef);
hipError_t hipTexRefGetMipMappedArray(hipMipmappedArray_t* pArray, const(textureReference)* texRef);
hipError_t hipTexRefSetAddress(size_t* ByteOffset, textureReference* texRef,
        hipDeviceptr_t dptr, size_t bytes);
hipError_t hipTexRefSetAddress2D(textureReference* texRef,
        const(HIP_ARRAY_DESCRIPTOR)* desc, hipDeviceptr_t dptr, size_t Pitch);
hipError_t hipTexRefSetMaxAnisotropy(textureReference* texRef, uint maxAniso);

hipError_t hipTexRefSetBorderColor(textureReference* texRef, float* pBorderColor);
hipError_t hipTexRefSetMipmapFilterMode(textureReference* texRef, hipTextureFilterMode fm);
hipError_t hipTexRefSetMipmapLevelBias(textureReference* texRef, float bias);
hipError_t hipTexRefSetMipmapLevelClamp(textureReference* texRef,
        float minMipMapLevelClamp, float maxMipMapLevelClamp);
hipError_t hipTexRefSetMipmappedArray(textureReference* texRef,
        hipMipmappedArray* mipmappedArray, uint Flags);
hipError_t hipMipmappedArrayCreate(hipMipmappedArray_t* pHandle,
        HIP_ARRAY3D_DESCRIPTOR* pMipmappedArrayDesc, uint numMipmapLevels);
hipError_t hipMipmappedArrayDestroy(hipMipmappedArray_t hMipmappedArray);
hipError_t hipMipmappedArrayGetLevel(hipArray_t* pLevelArray,
        hipMipmappedArray_t hMipMappedArray, uint level);

hipError_t hipRegisterApiCallback(uint id, void* fun, void* arg);
hipError_t hipRemoveApiCallback(uint id);
hipError_t hipRegisterActivityCallback(uint id, void* fun, void* arg);
hipError_t hipRemoveActivityCallback(uint id);
const(char)* hipApiName(uint id);
const(char)* hipKernelNameRef(const hipFunction_t f);
const(char)* hipKernelNameRefByPtr(const(void)* hostFunction, hipStream_t stream);
int hipGetStreamDeviceId(hipStream_t stream);

hipError_t hipStreamBeginCapture(hipStream_t stream, hipStreamCaptureMode mode);

hipError_t hipStreamEndCapture(hipStream_t stream, hipGraph_t* pGraph);

hipError_t hipStreamGetCaptureInfo(hipStream_t stream,
        hipStreamCaptureStatus* pCaptureStatus, ulong* pId);

hipError_t hipStreamGetCaptureInfo_v2(hipStream_t stream, hipStreamCaptureStatus* captureStatus_out, ulong* id_out,
        hipGraph_t* graph_out, const(hipGraphNode_t*)* dependencies_out,
        size_t* numDependencies_out);

hipError_t hipStreamIsCapturing(hipStream_t stream, hipStreamCaptureStatus* pCaptureStatus);

hipError_t hipStreamUpdateCaptureDependencies(hipStream_t stream,
        hipGraphNode_t* dependencies, size_t numDependencies, uint flags);

hipError_t hipLaunchHostFunc(hipStream_t stream, hipHostFn_t fn, void* userData);

hipError_t hipThreadExchangeStreamCaptureMode(hipStreamCaptureMode* mode);

hipError_t hipGraphCreate(hipGraph_t* pGraph, uint flags);

hipError_t hipGraphDestroy(hipGraph_t graph);

hipError_t hipGraphAddDependencies(hipGraph_t graph, const(hipGraphNode_t)* from,
        const(hipGraphNode_t)* to, size_t numDependencies);

hipError_t hipGraphRemoveDependencies(hipGraph_t graph,
        const(hipGraphNode_t)* from, const(hipGraphNode_t)* to, size_t numDependencies);

hipError_t hipGraphGetEdges(hipGraph_t graph, hipGraphNode_t* from,
        hipGraphNode_t* to, size_t* numEdges);

hipError_t hipGraphGetNodes(hipGraph_t graph, hipGraphNode_t* nodes, size_t* numNodes);

hipError_t hipGraphGetRootNodes(hipGraph_t graph, hipGraphNode_t* pRootNodes,
        size_t* pNumRootNodes);

hipError_t hipGraphNodeGetDependencies(hipGraphNode_t node,
        hipGraphNode_t* pDependencies, size_t* pNumDependencies);

hipError_t hipGraphNodeGetDependentNodes(hipGraphNode_t node,
        hipGraphNode_t* pDependentNodes, size_t* pNumDependentNodes);

hipError_t hipGraphNodeGetType(hipGraphNode_t node, hipGraphNodeType* pType);

hipError_t hipGraphDestroyNode(hipGraphNode_t node);

hipError_t hipGraphClone(hipGraph_t* pGraphClone, hipGraph_t originalGraph);

hipError_t hipGraphNodeFindInClone(hipGraphNode_t* pNode,
        hipGraphNode_t originalNode, hipGraph_t clonedGraph);

hipError_t hipGraphInstantiate(hipGraphExec_t* pGraphExec, hipGraph_t graph,
        hipGraphNode_t* pErrorNode, char* pLogBuffer, size_t bufferSize);

hipError_t hipGraphInstantiateWithFlags(hipGraphExec_t* pGraphExec, hipGraph_t graph, ulong flags);

hipError_t hipGraphLaunch(hipGraphExec_t graphExec, hipStream_t stream);

hipError_t hipGraphExecDestroy(hipGraphExec_t graphExec);

hipError_t hipGraphExecUpdate(hipGraphExec_t hGraphExec, hipGraph_t hGraph,
        hipGraphNode_t* hErrorNode_out, hipGraphExecUpdateResult* updateResult_out);

hipError_t hipGraphAddKernelNode(hipGraphNode_t* pGraphNode, hipGraph_t graph,
        const(hipGraphNode_t)* pDependencies, size_t numDependencies,
        const(hipKernelNodeParams)* pNodeParams);

hipError_t hipGraphKernelNodeGetParams(hipGraphNode_t node, hipKernelNodeParams* pNodeParams);

hipError_t hipGraphKernelNodeSetParams(hipGraphNode_t node, const(hipKernelNodeParams)* pNodeParams);

hipError_t hipGraphExecKernelNodeSetParams(hipGraphExec_t hGraphExec,
        hipGraphNode_t node, const(hipKernelNodeParams)* pNodeParams);

hipError_t hipGraphAddMemcpyNode(hipGraphNode_t* pGraphNode, hipGraph_t graph,
        const(hipGraphNode_t)* pDependencies, size_t numDependencies,
        const(hipMemcpy3DParms)* pCopyParams);

hipError_t hipGraphMemcpyNodeGetParams(hipGraphNode_t node, hipMemcpy3DParms* pNodeParams);

hipError_t hipGraphMemcpyNodeSetParams(hipGraphNode_t node, const(hipMemcpy3DParms)* pNodeParams);

hipError_t hipGraphKernelNodeSetAttribute(hipGraphNode_t hNode,
        hipKernelNodeAttrID attr, const(hipKernelNodeAttrValue)* value);

hipError_t hipGraphKernelNodeGetAttribute(hipGraphNode_t hNode,
        hipKernelNodeAttrID attr, hipKernelNodeAttrValue* value);

hipError_t hipGraphExecMemcpyNodeSetParams(hipGraphExec_t hGraphExec,
        hipGraphNode_t node, hipMemcpy3DParms* pNodeParams);

hipError_t hipGraphAddMemcpyNode1D(hipGraphNode_t* pGraphNode, hipGraph_t graph,
        const(hipGraphNode_t)* pDependencies, size_t numDependencies, void* dst,
        const(void)* src, size_t count, hipMemcpyKind kind);

hipError_t hipGraphMemcpyNodeSetParams1D(hipGraphNode_t node, void* dst,
        const(void)* src, size_t count, hipMemcpyKind kind);

hipError_t hipGraphExecMemcpyNodeSetParams1D(hipGraphExec_t hGraphExec,
        hipGraphNode_t node, void* dst, const(void)* src, size_t count, hipMemcpyKind kind);

hipError_t hipGraphAddMemcpyNodeFromSymbol(hipGraphNode_t* pGraphNode, hipGraph_t graph,
        const(hipGraphNode_t)* pDependencies, size_t numDependencies, void* dst,
        const(void)* symbol, size_t count, size_t offset, hipMemcpyKind kind);

hipError_t hipGraphMemcpyNodeSetParamsFromSymbol(hipGraphNode_t node, void* dst,
        const(void)* symbol, size_t count, size_t offset, hipMemcpyKind kind);

hipError_t hipGraphExecMemcpyNodeSetParamsFromSymbol(hipGraphExec_t hGraphExec, hipGraphNode_t node,
        void* dst, const(void)* symbol, size_t count, size_t offset, hipMemcpyKind kind);

hipError_t hipGraphAddMemcpyNodeToSymbol(hipGraphNode_t* pGraphNode, hipGraph_t graph,
        const(hipGraphNode_t)* pDependencies, size_t numDependencies, const(void)* symbol,
        const(void)* src, size_t count, size_t offset, hipMemcpyKind kind);

hipError_t hipGraphMemcpyNodeSetParamsToSymbol(hipGraphNode_t node,
        const(void)* symbol, const(void)* src, size_t count, size_t offset, hipMemcpyKind kind);

hipError_t hipGraphExecMemcpyNodeSetParamsToSymbol(hipGraphExec_t hGraphExec, hipGraphNode_t node,
        const(void)* symbol, const(void)* src, size_t count, size_t offset, hipMemcpyKind kind);

hipError_t hipGraphAddMemsetNode(hipGraphNode_t* pGraphNode, hipGraph_t graph,
        const(hipGraphNode_t)* pDependencies, size_t numDependencies,
        const(hipMemsetParams)* pMemsetParams);

hipError_t hipGraphMemsetNodeGetParams(hipGraphNode_t node, hipMemsetParams* pNodeParams);

hipError_t hipGraphMemsetNodeSetParams(hipGraphNode_t node, const(hipMemsetParams)* pNodeParams);

hipError_t hipGraphExecMemsetNodeSetParams(hipGraphExec_t hGraphExec,
        hipGraphNode_t node, const(hipMemsetParams)* pNodeParams);

hipError_t hipGraphAddHostNode(hipGraphNode_t* pGraphNode, hipGraph_t graph,
        const(hipGraphNode_t)* pDependencies, size_t numDependencies,
        const(hipHostNodeParams)* pNodeParams);

hipError_t hipGraphHostNodeGetParams(hipGraphNode_t node, hipHostNodeParams* pNodeParams);

hipError_t hipGraphHostNodeSetParams(hipGraphNode_t node, const(hipHostNodeParams)* pNodeParams);

hipError_t hipGraphExecHostNodeSetParams(hipGraphExec_t hGraphExec,
        hipGraphNode_t node, const(hipHostNodeParams)* pNodeParams);

hipError_t hipGraphAddChildGraphNode(hipGraphNode_t* pGraphNode, hipGraph_t graph,
        const(hipGraphNode_t)* pDependencies, size_t numDependencies, hipGraph_t childGraph);

hipError_t hipGraphChildGraphNodeGetGraph(hipGraphNode_t node, hipGraph_t* pGraph);

hipError_t hipGraphExecChildGraphNodeSetParams(hipGraphExec_t hGraphExec,
        hipGraphNode_t node, hipGraph_t childGraph);

hipError_t hipGraphAddEmptyNode(hipGraphNode_t* pGraphNode, hipGraph_t graph,
        const(hipGraphNode_t)* pDependencies, size_t numDependencies);

hipError_t hipGraphAddEventRecordNode(hipGraphNode_t* pGraphNode, hipGraph_t graph,
        const(hipGraphNode_t)* pDependencies, size_t numDependencies, hipEvent_t event);

hipError_t hipGraphEventRecordNodeGetEvent(hipGraphNode_t node, hipEvent_t* event_out);

hipError_t hipGraphEventRecordNodeSetEvent(hipGraphNode_t node, hipEvent_t event);

hipError_t hipGraphExecEventRecordNodeSetEvent(hipGraphExec_t hGraphExec,
        hipGraphNode_t hNode, hipEvent_t event);

hipError_t hipGraphAddEventWaitNode(hipGraphNode_t* pGraphNode, hipGraph_t graph,
        const(hipGraphNode_t)* pDependencies, size_t numDependencies, hipEvent_t event);

hipError_t hipGraphEventWaitNodeGetEvent(hipGraphNode_t node, hipEvent_t* event_out);

hipError_t hipGraphEventWaitNodeSetEvent(hipGraphNode_t node, hipEvent_t event);

hipError_t hipGraphExecEventWaitNodeSetEvent(hipGraphExec_t hGraphExec,
        hipGraphNode_t hNode, hipEvent_t event);

struct hipMemAllocationProp
{
    ubyte compressionType;
    hipMemLocation location;
    hipMemAllocationHandleType requestedHandleType;
    hipMemAllocationType type;
    ushort usage;
    void* win32HandleMetaData;
}

struct ihipMemGenericAllocationHandle;
alias hipMemGenericAllocationHandle_t = ihipMemGenericAllocationHandle*;

enum hipMemAllocationGranularity_flags
{
    hipMemAllocationGranularityMinimum = 0x0,
    hipMemAllocationGranularityRecommended = 0x1
}

enum hipMemHandleType
{
    hipMemHandleTypeGeneric = 0x0
}

enum hipMemOperationType
{
    hipMemOperationTypeMap = 0x1,
    hipMemOperationTypeUnmap = 0x2
}

enum hipArraySparseSubresourceType
{
    hipArraySparseSubresourceTypeSparseLevel = 0x0,
    hipArraySparseSubresourceTypeMiptail = 0x1
}

struct hipArrayMapInfo
{
    hipResourceType resourceType;

    union _Anonymous_21
    {
        hipMipmappedArray mipmap;
        hipArray_t array;
    }

    _Anonymous_21 resource;
    hipArraySparseSubresourceType subresourceType;

    union _Anonymous_22
    {
        struct _Anonymous_23
        {
            uint level;
            uint layer;
            uint offsetX;
            uint offsetY;
            uint offsetZ;
            uint extentWidth;
            uint extentHeight;
            uint extentDepth;
        }

        _Anonymous_23 sparseLevel;

        struct _Anonymous_24
        {
            uint layer;
            ulong offset;
            ulong size;
        }

        _Anonymous_24 miptail;
    }

    _Anonymous_22 subresource;
    hipMemOperationType memOperationType;
    hipMemHandleType memHandleType;

    union _Anonymous_25
    {
        hipMemGenericAllocationHandle_t memHandle;
    }

    _Anonymous_25 memHandle;
    ulong offset;
    uint deviceBitMask;
    uint flags;
    uint[2] reserved;
}

hipError_t hipMemAddressFree(void* devPtr, size_t size);

hipError_t hipMemAddressReserve(void** ptr, size_t size, size_t alignment, void* addr, ulong flags);

hipError_t hipMemCreate(hipMemGenericAllocationHandle_t* handle, size_t size,
        const(hipMemAllocationProp)* prop, ulong flags);

hipError_t hipMemExportToShareableHandle(void* shareableHandle,
        hipMemGenericAllocationHandle_t handle, hipMemAllocationHandleType handleType, ulong flags);

hipError_t hipMemGetAccess(ulong* flags, const(hipMemLocation)* location, void* ptr);

hipError_t hipMemGetAllocationGranularity(size_t* granularity,
        const(hipMemAllocationProp)* prop, hipMemAllocationGranularity_flags option);

hipError_t hipMemGetAllocationPropertiesFromHandle(hipMemAllocationProp* prop,
        hipMemGenericAllocationHandle_t handle);

hipError_t hipMemImportFromShareableHandle(hipMemGenericAllocationHandle_t* handle,
        void* osHandle, hipMemAllocationHandleType shHandleType);

hipError_t hipMemMap(void* ptr, size_t size, size_t offset,
        hipMemGenericAllocationHandle_t handle, ulong flags);

hipError_t hipMemMapArrayAsync(hipArrayMapInfo* mapInfoList, uint count, hipStream_t stream);

hipError_t hipMemRelease(hipMemGenericAllocationHandle_t handle);

hipError_t hipMemRetainAllocationHandle(hipMemGenericAllocationHandle_t* handle, void* addr);

hipError_t hipMemSetAccess(void* ptr, size_t size, const(hipMemAccessDesc)* desc, size_t count);

hipError_t hipMemUnmap(void* ptr, size_t size);

alias GLuint = uint;
alias GLenum = uint;

hipError_t hipGLGetDevices(uint* pHipDeviceCount, int* pHipDevices,
        uint hipDeviceCount, hipGLDeviceList deviceList);

hipError_t hipGraphicsGLRegisterBuffer(hipGraphicsResource** resource, GLuint buffer, uint flags);

hipError_t hipGraphicsGLRegisterImage(hipGraphicsResource** resource,
        GLuint image, GLenum target, uint flags);

hipError_t hipGraphicsMapResources(int count, hipGraphicsResource_t* resources, hipStream_t stream);

hipError_t hipGraphicsSubResourceGetMappedArray(hipArray_t* array,
        hipGraphicsResource_t resource, uint arrayIndex, uint mipLevel);

hipError_t hipGraphicsResourceGetMappedPointer(void** devPtr, size_t* size,
        hipGraphicsResource_t resource);

hipError_t hipGraphicsUnmapResources(int count, hipGraphicsResource_t* resources, hipStream_t stream);

hipError_t hipGraphicsUnregisterResource(hipGraphicsResource_t resource);
