module rocm.utils;

import rocm.hip_runtime_api;

void hipassert(hipError_t error) @safe @nogc nothrow
{
    assert(error == HIP_SUCCESS);
}
