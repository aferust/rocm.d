import std.stdio;

import rocm.hip_runtime_api;
import rocm.utils;

void main()
{
    int id;
    hipassert(hipGetDevice(&id));
    writeln(id);
}
